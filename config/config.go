package config

import (
	"bitbucket.org/canopei/golibs/auth"
	"bitbucket.org/canopei/golibs/logging"
	"github.com/BurntSushi/toml"
)

const (
	// GrantTypeAuthorizationCode is the RFC value
	GrantTypeAuthorizationCode = "authorization_code"

	// GrantTypeImplicit is the RFC value
	GrantTypeImplicit = "implicit"

	// GrantTypePassword is the RFC value
	GrantTypePassword = "password"

	// GrantTypeRefreshToken is the RFC value
	GrantTypeRefreshToken = "refresh_token"
)

var (
	// ValidGrantTypes is the list of valid grant types
	ValidGrantTypes = [...]string{GrantTypeAuthorizationCode, GrantTypeImplicit, GrantTypePassword, GrantTypeRefreshToken}
)

// ServiceConfig holds the general configuration for the service
type ServiceConfig struct {
	Name       string
	Env        string
	ApiPort    int16 `toml:"api_port"`
	GrpcPort   int16 `toml:"grpc_port"`
	HealthPort int16 `toml:"health_port"`
}

// DatabaseConfig holds the database configuration
type DatabaseConfig struct {
	Addr     string
	Username string
	Password string
	DbName   string `toml:"dbname"`
}

// GrpcConfig holds the Grpc configuration
type GrpcConfig struct {
	AccountAddr string `toml:"account_addr"`
}

// ClientConfig holds the client configuration
type ClientConfig struct {
	SecretLength int `toml:"secret_length"`
}

// Config holds the entire configuration
type Config struct {
	Service  ServiceConfig          `toml:"service"`
	Db       DatabaseConfig         `toml:"database"`
	Grpc     GrpcConfig             `toml:"grpc"`
	Client   ClientConfig           `toml:"client"`
	Auth     auth.Config            `toml:"auth"`
	Logstash logging.LogstashConfig `toml:"logstash"`
}

// LoadConfig loads the configuration from the given filepath
func LoadConfig(filePath string) (*Config, error) {
	var conf Config
	_, err := toml.DecodeFile(filePath, &conf)

	return &conf, err
}

// IsGrantTypeValid checks if the given grant is valid
func IsGrantTypeValid(grant string) bool {
	for _, v := range ValidGrantTypes {
		if v == grant {
			return true
		}
	}
	return false
}
