<project name="SNG API Auth" default="prepare" basedir=".">
    <taskdef resource="net/sf/antcontrib/antlib.xml" />

    <property name="componentName" value="auth" />
    <property name="basedir" value="${project.basedir}" />

    <property file="sng-api-staging.properties" prefix="sng-staging." />
    <property file="sng-api-production.properties" prefix="sng-production." />

    <target name="clean" description="Clean up and create artifact directory">
        <delete dir="build"/>
        <mkdir dir="build"/>
    </target>

    <target name="configs">
        <propertyselector property="config.list" match="sng\-staging\.(.*)" select="\1" delimiter="," casesensitive="false" />
        <copy file="config.toml.dist" tofile="config-staging.toml" overwrite="true" />
        <foreach list="${config.list}" delimiter="," target="configReplace" param="config.key">
            <param name="env" value="staging"/>
            <param name="configKey" value="${config.key}"/>
        </foreach>

        <propertyselector property="config.list" match="sng\-production\.(.*)" select="\1" delimiter="," casesensitive="false" />
        <copy file="config.toml.dist" tofile="config-production.toml" overwrite="true" />
        <foreach list="${config.list}" delimiter="," target="configReplace" param="config.key">
            <param name="env" value="production"/>
        </foreach>
    </target>

    <target name="configReplace">
        <propertycopy property="config.value" from="sng-${env}.${config.key}" />
        <replace file="config-${env}.toml">
            <replacefilter token="@${config.key}@" value="${config.value}" />
        </replace>
    </target>

    <target name="deps">
        <exec executable="gcloud" failonerror="true">
            <arg value="docker"/>
            <arg value="--"/>
            <arg value="pull"/>
            <arg value="us.gcr.io/eastern-augury-170215/sng-godev:latest"/>
        </exec>
    </target>

    <target name="prepare" depends="clean,configs,deps">
        <property name="gitRef" value="${GIT_REF}" />

        <echo message="Got Git ref: ${gitRef}" />
        <if>
            <matches pattern="^v[0-9]+" string="${gitRef}"/>
            <then>
                <propertyregex property="app.version"
                    input="${gitRef}"
                    regexp="^v"
                    replace=""
                    global="true" />
            </then>
            <else>
                <shellscript shell="bash" outputproperty="app.version" failonerror="true">
                    T_GIT_COMMIT=${gitCommit}
                    echo ${T_GIT_COMMIT:0:8}
                </shellscript>
            </else>
        </if>
        <echo message="The VERSION is '${app.version}'." />
        <echo file="VERSION">${app.version}</echo>

        <tar destfile="build/prepared.tar.gz" compression="gzip">
            <tarfileset dir="${basedir}">
                <exclude name="config.toml.dist" />
            </tarfileset>
        </tar>
    </target>

    <target name="test">
        <shellscript shell="bash" failonerror="true">
            docker run -t -P -v ~/glidecache:/root/.glide -v `pwd`:/go/src/bitbucket.org/canopei/${componentName} -v ~/.ssh/id_rsa:/root/.ssh/id_rsa us.gcr.io/eastern-augury-170215/sng-godev:latest bash -c 'cd /go/src/bitbucket.org/canopei/${componentName}; glide --no-color up &amp;&amp; cd server &amp;&amp; go test -v --cover'
        </shellscript>

        <tar destfile="build/tested.tar.gz" compression="gzip">
            <tarfileset dir="${basedir}" />
        </tar>
    </target>

    <target name="build">
        <copy file="config-staging.toml" tofile="build/config-staging.toml" />
        <copy file="config-production.toml" tofile="build/config-production.toml" />
        <copy file="VERSION" tofile="build/VERSION" />

        <loadfile property="app.version" srcFile="VERSION"/>

        <shellscript shell="bash" failonerror="true">
            docker run -t -P -v `pwd`:/go/src/bitbucket.org/canopei/${componentName} -v ~/.ssh/id_rsa:/root/.ssh/id_rsa us.gcr.io/eastern-augury-170215/sng-godev:latest bash -c 'cd /go/src/bitbucket.org/canopei/${componentName}; go build -o build/server server/*.go &amp;&amp; go build -o build/api api/*.go'

            docker build -t us.gcr.io/eastern-augury-170215/sng-${componentName}:latest .
            gcloud docker -- push us.gcr.io/eastern-augury-170215/sng-${componentName}:latest
            gcloud container images add-tag us.gcr.io/eastern-augury-170215/sng-${componentName}:latest us.gcr.io/eastern-augury-170215/sng-${componentName}:${app.version}
        </shellscript>

        <tar destfile="build/build.tar.gz" compression="gzip">
            <tarfileset dir="${basedir}/build"/>
        </tar>
    </target>

    <target name="deploy-staging">
        <shellscript shell="bash" failonerror="true">
            gcloud container clusters get-credentials api-cluster-1 --zone us-central1-c

            kubectl --namespace=sng-staging set image deployment/sng-${componentName} sng-${componentName}=us.gcr.io/eastern-augury-170215/sng-${componentName}:${imageVersion}
            kubectl --namespace=sng-staging rollout status deployment/sng-${componentName}
        </shellscript>
    </target>

    <target name="deploy-production">
        <shellscript shell="bash" failonerror="true">
            gcloud container clusters get-credentials api-cluster-1 --zone us-central1-c

            kubectl --namespace=sng-production set image deployment/sng-${componentName} sng-${componentName}=us.gcr.io/eastern-augury-170215/sng-${componentName}:${imageVersion}
            kubectl --namespace=sng-production rollout status deployment/sng-${componentName}
        </shellscript>
    </target>
</project>