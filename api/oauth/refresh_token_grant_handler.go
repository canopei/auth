package oauth

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/Sirupsen/logrus"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	accountPb "bitbucket.org/canopei/account/protobuf"
	authPb "bitbucket.org/canopei/auth/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
)

const (
	// RenewRefreshTokenInterval represents the interval in which a refresh access token request
	// will also renew the refresh token
	RenewRefreshTokenInterval = 3 * 24 * time.Hour

	// RefreshGrantAccessTokenTTL is the TTL of the access tokens generated for a Refresh grant
	// type request
	RefreshGrantAccessTokenTTL = 3600
)

// HandleRefreshTokenGrantRequest handles the authorization for a Refresh grant type request
func (s *Server) HandleRefreshTokenGrantRequest(res http.ResponseWriter, req *http.Request, logger *logrus.Entry, request *AccessTokenRequest, client *authPb.Client) {
	if request.RefreshToken == "" {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Empty refresh token.")
		return
	}

	grpcContext := grpcUtils.GetContextForRequest(context.Background(), req)

	// Validate the code
	refreshToken, err := s.AuthService.GetRefreshToken(grpcContext, &authPb.GetRefreshTokenRequest{Token: request.RefreshToken})
	if err != nil {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidClient, "Invalid refresh token - not found in DB.")
		return
	}
	// Check the code
	if refreshToken.ClientId != request.ClientID {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid refresh token - client ID.")
		return
	}
	expiresAt, _ := time.Parse(SQLDateTimeFormat, refreshToken.ExpiresAt)
	if expiresAt.Before(time.Now()) {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid refresh token - expired.")
		return
	}

	// Get the account
	account, err := s.AccountService.GetAccount(grpcContext, &accountPb.GetAccountRequest{Id: refreshToken.AccountId})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.NotFound {
			s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, fmt.Sprintf("Account not found '%s'.", request.Username))
		} else {
			logger.Errorf("Cannot fetch account '%s': %v", request.Username, err)
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	var permissions []*authPb.Permission
	permissionsList, err := s.AccountService.GetAccountPermissions(grpcContext, &accountPb.GetAccountPermissionsRequest{
		Uuid:   account.Uuid,
		SiteId: client.SiteId,
	})
	if err != nil {
		logger.Errorf("Cannot fetch account permissions for '%s', site ID %d: %v", account.Uuid, client.SiteId, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	for _, perm := range permissionsList.Permissions {
		permissions = append(permissions, &authPb.Permission{SiteId: perm.SiteId, Code: perm.Code})
	}
	logger.Debugf("Found permissions: %v.", permissions)

	// If the refresh token is close to expiration, generate a new one
	if expiresAt.Before(time.Now().Add(RenewRefreshTokenInterval)) {
		// Create a new refresh token
		newRefreshToken, err := s.AuthService.CreateRefreshToken(grpcContext, &authPb.CreateRefreshTokenRequest{
			ClientId:  client.Id,
			AccountId: account.Id,
			ExpiresIn: RefreshTokenTTL,
		})
		if err != nil {
			logger.Errorf("Cannot create refresh token for '%s', client ID %d: %v", account.Uuid, client.Id, err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}

		// Revoke the old token
		_, err = s.AuthService.RevokeRefreshToken(grpcContext, &authPb.RefreshToken{Token: refreshToken.Token})
		if err != nil {
			logger.Errorf("Cannot revoke the old refresh token '%s': %v", refreshToken.Token, err)
			// Do not send the new refresh token and revoke it if we couldn't revoke the old one
			_, err = s.AuthService.RevokeRefreshToken(grpcContext, &authPb.RefreshToken{Token: refreshToken.Token})
			if err != nil {
				logger.Errorf("Cannot revoke the new refresh token '%s': %v", newRefreshToken.Token, err)
			}
		} else {
			refreshToken = newRefreshToken
		}
	}

	// Create an access token
	accessToken, err := s.AuthService.CreateAccessToken(grpcContext, &authPb.CreateAccessTokenRequest{
		ClientId:           client.Id,
		AccountUuid:        account.Uuid,
		AccountPermissions: permissions,
		ExpiresIn:          RefreshGrantAccessTokenTTL,
	})
	if err != nil {
		logger.Errorf("Cannot create access token for '%s', client ID %d: %v", account.Uuid, client.Id, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	response := &AccessTokenResponse{
		AccessToken:  accessToken.Token,
		TokenType:    "Bearer",
		ExpiresIn:    accessToken.ExpiresIn,
		RefreshToken: refreshToken.Token,
		Permissions:  permissions,
	}
	logger.Debugf("Generated access token: %#v.", response)

	s.writeJSONResponse(res, response)
}
