package oauth

import "github.com/gorilla/mux"

// RegisterOauthRoutes registers the routes to a mux router
func (s *Server) RegisterOauthRoutes(r *mux.Router) {
	r.HandleFunc("/oauth2/token", s.HandleTokenRequest).Methods("POST")
	r.HandleFunc("/oauth2/me", s.HandleResourceOwnerDetailsRequest).Methods("GET")
}
