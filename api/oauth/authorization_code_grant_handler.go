package oauth

import (
	"context"
	"fmt"
	"net/http"

	"github.com/Sirupsen/logrus"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"time"

	accountPb "bitbucket.org/canopei/account/protobuf"
	authPb "bitbucket.org/canopei/auth/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
)

const (
	// CodeGrantAccessTokenTTL in seconds
	CodeGrantAccessTokenTTL = 3600
)

// HandleAuthorizationCodeGrantRequest handles the authorization for a Code grant type request
func (s *Server) HandleAuthorizationCodeGrantRequest(res http.ResponseWriter, req *http.Request, logger *logrus.Entry, request *AccessTokenRequest, client *authPb.Client) {
	if request.Code == "" {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Empty code.")
		return
	}
	if request.RedirectURI == "" {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Empty redirect URI.")
		return
	}

	grpcContext := grpcUtils.GetContextForRequest(context.Background(), req)

	// Validate the code
	authorizationCode, err := s.AuthService.GetAuthorizationCode(grpcContext, &authPb.GetAuthorizationCodeRequest{Code: request.Code})
	if err != nil {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidClient, "Invalid authorization code - not found in DB.")
		return
	}
	// Check the code
	if authorizationCode.ClientId != request.ClientID {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid authorization code - client ID.")
		return
	}
	if authorizationCode.RedirectUri != request.RedirectURI {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid authorization code - redirect URI.")
		return
	}
	expiresAt, _ := time.Parse(SQLDateTimeFormat, authorizationCode.ExpiresAt)
	if expiresAt.Before(time.Now()) {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid authorization code - expired.")
		return
	}

	// Get the account
	account, err := s.AccountService.GetAccount(grpcContext, &accountPb.GetAccountRequest{Id: authorizationCode.AccountId})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.NotFound {
			s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, fmt.Sprintf("Account not found %d.", authorizationCode.AccountId))
		} else {
			logger.Errorf("Cannot fetch account '%s': %v", request.Username, err)
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	var permissions []*authPb.Permission
	permissionsList, err := s.AccountService.GetAccountPermissions(grpcContext, &accountPb.GetAccountPermissionsRequest{
		Uuid:   account.Uuid,
		SiteId: client.SiteId,
	})
	if err != nil {
		logger.Errorf("Cannot fetch account permissions for '%s', site ID %d: %v", account.Uuid, client.SiteId, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	for _, perm := range permissionsList.Permissions {
		permissions = append(permissions, &authPb.Permission{SiteId: perm.SiteId, Code: perm.Code})
	}
	logger.Debugf("Found permissions: %v.", permissions)

	// Create a refresh token
	refreshToken, err := s.AuthService.CreateRefreshToken(grpcContext, &authPb.CreateRefreshTokenRequest{
		ClientId:  client.Id,
		AccountId: account.Id,
		ExpiresIn: RefreshTokenTTL,
	})
	if err != nil {
		logger.Errorf("Cannot create refresh token for '%s', client ID %d: %v", account.Uuid, client.Id, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Create an access token
	accessToken, err := s.AuthService.CreateAccessToken(grpcContext, &authPb.CreateAccessTokenRequest{
		ClientId:           client.Id,
		AccountUuid:        account.Uuid,
		AccountPermissions: permissions,
		ExpiresIn:          CodeGrantAccessTokenTTL,
	})
	if err != nil {
		logger.Errorf("Cannot create access token for '%s', client ID %d: %v", account.Uuid, client.Id, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, err = s.AuthService.RevokeAuthorizationCode(grpcContext, authorizationCode)
	if err != nil {
		logger.Errorf("Failed to revoke authorization code '%s': %v", authorizationCode.Code, err)
	}

	response := &AccessTokenResponse{
		AccessToken:  accessToken.Token,
		TokenType:    "Bearer",
		ExpiresIn:    accessToken.ExpiresIn,
		RefreshToken: refreshToken.Token,
		Permissions:  permissions,
	}
	logger.Debugf("Generated access token: %#v.", response)

	s.writeJSONResponse(res, response)
}
