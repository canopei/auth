package oauth

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/Sirupsen/logrus"
)

func (s *Server) handleTokenRequestError(logger *logrus.Entry, res http.ResponseWriter, errorCode string, errorMessage string) {
	logger.Error(errorMessage)

	res.WriteHeader(http.StatusBadRequest)
	s.writeJSONResponse(res, &AccessTokenResponse{Error: errorCode})
}

func (s *Server) writeJSONResponse(res http.ResponseWriter, response interface{}) error {
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		s.Logger.Error("Failed to Marshal the response.")
		res.WriteHeader(http.StatusInternalServerError)
		return err
	}

	res.Write([]byte(jsonResponse))
	return nil
}

// GetRequestLogger builds an instance of a logger for the current request using an original logger
// It adds a requestid Field to the logger
func GetRequestLogger(req *http.Request, originalLogger *logrus.Entry) *logrus.Entry {
	log := logrus.New()
	log.Formatter = originalLogger.Logger.Formatter
	log.Level = originalLogger.Logger.Level
	log.Hooks = originalLogger.Logger.Hooks
	newLogger := log.WithFields(originalLogger.Data)

	requestID := req.Header.Get("X-Request-Id")
	if requestID != "" {
		newLogger.Data["requestid"] = requestID
	}

	return newLogger
}

func timeTrack(logger *logrus.Entry, start time.Time, name string) {
	elapsed := time.Since(start)
	logger.Debugf("%s took %s", name, elapsed)
}
