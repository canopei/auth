package oauth

import (
	"context"
	"net/http"
	"strconv"
	"strings"
	"time"

	"google.golang.org/grpc"

	"fmt"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/auth/config"
	authPb "bitbucket.org/canopei/auth/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/Sirupsen/logrus"
	jwt "github.com/dgrijalva/jwt-go"
	"google.golang.org/grpc/codes"
)

const (
	// OauthErrorInvalidRequest is the invalid_request error text
	OauthErrorInvalidRequest = "invalid_request"

	// OauthErrorInvalidClient is the invalid_client error text
	OauthErrorInvalidClient = "invalid_client"

	// OauthErrorInvalidGrant is the invalid_grant error text
	OauthErrorInvalidGrant = "invalid_grant"

	// OauthErrorUnauthorizedClient is the unauthorized_client error text
	OauthErrorUnauthorizedClient = "unauthorized_client"

	// OauthErrorAccessDenied is the access_denied error text
	OauthErrorAccessDenied = "access_denied"

	// SQLDateTimeFormat is the layout for DATETIME sql type
	SQLDateTimeFormat = "2006-01-02 15:04:05"

	// RefreshTokenTTL in seconds
	RefreshTokenTTL = 60 * 24 * 3600
)

// Server holds and OAuth2 server
type Server struct {
	Logger         *logrus.Entry
	AccountService accountPb.AccountServiceClient
	ClientService  authPb.ClientServiceClient
	AuthService    authPb.AuthServiceClient
	JWTSigningKey  string
}

// AccessTokenRequest holds the request data for an access token
type AccessTokenRequest struct {
	GrantType    string
	Code         string
	Username     string
	Password     string
	ClientID     int32
	ClientSecret string
	RedirectURI  string
	RefreshToken string
}

// AccessTokenResponse holds the response of an Access token request
type AccessTokenResponse struct {
	AccessToken  string               `json:"access_token,omitempty"`
	TokenType    string               `json:"token_type,omitempty"`
	ExpiresIn    int32                `json:"expires_in,omitempty"`
	Permissions  []*authPb.Permission `json:"permissions,omitempty"`
	RefreshToken string               `json:"refresh_token,omitempty"`
	Error        string               `json:"error,omitempty"`
}

// NewServer creates an instance of an OauthServer
func NewServer(
	logger *logrus.Entry,
	accountService accountPb.AccountServiceClient,
	clientService authPb.ClientServiceClient,
	authService authPb.AuthServiceClient,
	jwtSigningKey string,
) *Server {
	return &Server{
		Logger:         logger,
		AccountService: accountService,
		ClientService:  clientService,
		AuthService:    authService,
		JWTSigningKey:  jwtSigningKey,
	}
}

// HandleResourceOwnerDetailsRequest handles the resource owner details request
func (s *Server) HandleResourceOwnerDetailsRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	logger.Info("HandleResourceOwnerDetailsRequest")
	defer timeTrack(logger, time.Now(), "HandleResourceOwnerDetailsRequest")

	authorizationToken := req.Header.Get("Authorization")
	if authorizationToken == "" {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Empty Authorization header.")
		return
	}
	s.Logger.Debugf("Got Authorization: %s", authorizationToken)

	authorizationParts := strings.Split(authorizationToken, " ")
	if len(authorizationParts) != 2 {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid Authorization header.")
		return
	}
	accessToken := strings.TrimSpace(authorizationParts[1])
	if accessToken == "" {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid Authorization token.")
		return
	}

	claims, err := s.ExtractJWTClaims(accessToken)
	if err != nil {
		logger.Warningf("Unable to decode the access token '%s'.", accessToken)
	} else {
		accountUUID, ok := claims["account_uuid"].(string)
		if !ok {
			s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid Authorization token.")
			return
		}

		account, err := s.AccountService.GetAccount(context.Background(), &accountPb.GetAccountRequest{Search: accountUUID})
		if err != nil {
			if grpc.Code(err) == codes.NotFound {
				s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid Authorization token.")
				return
			}

			logger.Errorf("Cannot fetch account by UUID '%s': %v", accountUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}

		s.writeJSONResponse(res, account)
	}
}

// HandleTokenRequest handles the request for an access token
func (s *Server) HandleTokenRequest(res http.ResponseWriter, req *http.Request) {
	req.ParseForm()

	clientID, _ := strconv.ParseInt(req.PostFormValue("client_id"), 10, 32)
	request := &AccessTokenRequest{
		GrantType:    req.PostFormValue("grant_type"),
		Code:         req.PostFormValue("code"),
		ClientSecret: req.PostFormValue("client_secret"),
		Username:     req.PostFormValue("username"),
		Password:     req.PostFormValue("password"),
		ClientID:     int32(clientID),
		RedirectURI:  req.PostFormValue("redirect_uri"),
		RefreshToken: req.PostFormValue("refresh_token"),
	}

	logger := GetRequestLogger(req, s.Logger)

	logger.Infof("HandleTokenRequest: %#v", request)
	defer timeTrack(logger, time.Now(), "HandleTokenRequest")

	// Validations
	if request.ClientID < 1 {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Invalid client ID.")
		return
	}
	if request.GrantType == "" {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Empty grant type.")
		return
	}
	if !config.IsGrantTypeValid(request.GrantType) {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidGrant, fmt.Sprintf("Invalid grant type '%s'.", request.GrantType))
		return
	}

	var err error
	var grpcContext = grpcUtils.GetContextForRequest(context.Background(), req)

	// Validate client
	client, err := s.ClientService.GetClientByID(grpcContext, &authPb.GetClientByIDRequest{Id: request.ClientID})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.NotFound {
			s.handleTokenRequestError(logger, res, OauthErrorInvalidClient, fmt.Sprintf("Client not found %d.", request.ClientID))
		} else {
			logger.Errorf("Cannot fetch client by ID '%d': %v", request.ClientID, err)
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	switch request.GrantType {
	case config.GrantTypePassword:
		s.HandlePasswordGrantRequest(res, req, logger, request, client)
		return
	case config.GrantTypeAuthorizationCode:
		s.HandleAuthorizationCodeGrantRequest(res, req, logger, request, client)
		return
	case config.GrantTypeRefreshToken:
		s.HandleRefreshTokenGrantRequest(res, req, logger, request, client)
		return
	}

	res.WriteHeader(http.StatusNotFound)
}

// ExtractJWTClaims decrypts a JWT string and extracts the Claims
func (s *Server) ExtractJWTClaims(jwtTokenString string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(jwtTokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(s.JWTSigningKey), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}

	return nil, err
}
