package oauth

import (
	"context"
	"fmt"
	"net/http"

	"github.com/Sirupsen/logrus"

	accountPb "bitbucket.org/canopei/account/protobuf"
	authPb "bitbucket.org/canopei/auth/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// PassGrantAccessTokenTTL in seconds
	PassGrantAccessTokenTTL = 30 * 24 * 3600
)

// HandlePasswordGrantRequest handles the authorization for a Password grant type request
func (s *Server) HandlePasswordGrantRequest(res http.ResponseWriter, req *http.Request, logger *logrus.Entry, request *AccessTokenRequest, client *authPb.Client) {
	if request.Username == "" || request.Password == "" {
		s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, "Empty username or password.")
		return
	}

	// Validate client credentials
	if client.Secret != request.ClientSecret {
		s.handleTokenRequestError(logger, res, OauthErrorUnauthorizedClient, fmt.Sprintf("Wrong client secret '%s' for client '%d'.", request.ClientSecret, request.ClientID))
		return
	}

	grpcContext := grpcUtils.GetContextForRequest(context.Background(), req)

	account, err := s.AccountService.GetAccount(grpcContext, &accountPb.GetAccountRequest{Search: request.Username})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.NotFound {
			s.handleTokenRequestError(logger, res, OauthErrorInvalidRequest, fmt.Sprintf("Account not found '%s'.", request.Username))
		} else {
			logger.Errorf("Cannot fetch account '%s': %v", request.Username, err)
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// validate the password
	passwordHash := crypto.HashPasswordWithSalt(request.Password, account.PasswordSalt)
	if passwordHash != account.PasswordHash {
		s.handleTokenRequestError(logger, res, OauthErrorAccessDenied, "Invalid password.")
		res.WriteHeader(http.StatusUnauthorized)
		return
	}

	var permissions []*authPb.Permission
	permissionsList, err := s.AccountService.GetAccountPermissions(grpcContext, &accountPb.GetAccountPermissionsRequest{
		Uuid:   account.Uuid,
		SiteId: client.SiteId,
	})
	if err != nil {
		logger.Errorf("Cannot fetch account permissions for '%s', site ID %d: %v", account.Uuid, client.SiteId, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	for _, perm := range permissionsList.Permissions {
		permissions = append(permissions, &authPb.Permission{SiteId: perm.SiteId, Code: perm.Code})
	}
	logger.Debugf("Found permissions: %v.", permissions)

	token, err := s.AuthService.CreateAccessToken(grpcContext, &authPb.CreateAccessTokenRequest{
		ClientId:           client.Id,
		AccountUuid:        account.Uuid,
		AccountPermissions: permissions,
		ExpiresIn:          PassGrantAccessTokenTTL,
	})
	if err != nil {
		logger.Errorf("Cannot create access token for '%s', client ID %d: %v", account.Uuid, client.Id, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	response := &AccessTokenResponse{
		AccessToken: token.Token,
		TokenType:   "Bearer",
		ExpiresIn:   token.ExpiresIn,
		Permissions: permissions,
	}
	logger.Debugf("Generated access token: %#v.", response)

	s.writeJSONResponse(res, response)
}
