package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"google.golang.org/grpc"

	"bitbucket.org/canopei/account"
	accountPb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/auth"
	"bitbucket.org/canopei/auth/api/oauth"
	"bitbucket.org/canopei/auth/config"
	authPb "bitbucket.org/canopei/auth/protobuf"
	cApi "bitbucket.org/canopei/golibs/api"
	cGrpc "bitbucket.org/canopei/golibs/grpc/utils"
	"bitbucket.org/canopei/golibs/healthcheck"
	cHttp "bitbucket.org/canopei/golibs/http"
	"bitbucket.org/canopei/golibs/logging"
	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/urfave/negroni"
)

const (
	swaggerFile = "static/auth.swagger.json"
)

var (
	conf           *config.Config
	logger         *logrus.Entry
	accountService accountPb.AccountServiceClient
	clientService  authPb.ClientServiceClient
	authService    authPb.AuthServiceClient
	version        string
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := mux.NewRouter()
	mux.HandleFunc(healthcheck.Healthpath, HealthcheckHandler)

	oauthServer := oauth.NewServer(logger, accountService, clientService, authService, conf.Auth.JWTSigningKey)
	oauthServer.RegisterOauthRoutes(mux)

	gwmux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONBuiltin{}))
	opts := []grpc.DialOption{grpc.WithInsecure()}

	err := authPb.RegisterAuthServiceHandlerFromEndpoint(ctx, gwmux, fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort), opts)
	if err != nil {
		return err
	}
	mux.PathPrefix("/").Handler(cGrpc.RestGatewayResponseInterceptor(gwmux))

	n := negroni.New()
	n.Use(cHttp.NewXRequestIDMiddleware(16))
	n.Use(cApi.NewServiceAPIMiddleware(logger))
	n.Use(cApi.NewCombinedLoggingMiddleware(logger, mux, handlers.CombinedLoggingHandler))
	n.Use(negroni.NewRecovery())

	logger.Infof("Starting to serve on %d...", conf.Service.ApiPort)
	return http.ListenAndServe(fmt.Sprintf(":%d", conf.Service.ApiPort), n)
}

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("AUTH_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "api",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' API (%s)...", conf.Service.Name, version)

	accountService, _, err = account.NewAccountClient(conf.Grpc.AccountAddr)
	if err != nil {
		logger.Fatalf("Cannot start the account service: %v", err)
	}

	clientService, _, err = auth.NewClientClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot start the client service: %v", err)
	}

	authService, _, err = auth.NewAuthClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot start the auth service: %v", err)
	}

	if err := run(); err != nil {
		logger.Fatal(err)
	}
}

// HealthcheckHandler handle the healthcheck request
func HealthcheckHandler(res http.ResponseWriter, req *http.Request) {
	localLogger := GetRequestLogger(req, logger)

	_, err := authService.Ping(context.Background(), &empty.Empty{})
	if err != nil {
		localLogger.Errorf("Ping error: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")
	msg, _ := json.Marshal(map[string]string{"status": "ok", "version": version})
	res.Write([]byte(msg))
	return
}

// GetRequestLogger builds an instance of a logger for the current request using an original logger
// It adds a requestid Field to the logger
func GetRequestLogger(req *http.Request, originalLogger *logrus.Entry) *logrus.Entry {
	log := logrus.New()
	log.Formatter = originalLogger.Logger.Formatter
	log.Level = originalLogger.Logger.Level
	log.Hooks = originalLogger.Logger.Hooks
	newLogger := log.WithFields(originalLogger.Data)

	requestID := req.Header.Get("X-Request-Id")
	if requestID != "" {
		newLogger.Data["requestid"] = requestID
	}

	return newLogger
}
