package auth

import (
	"fmt"

	pb "bitbucket.org/canopei/auth/protobuf"
	"google.golang.org/grpc"
)

// NewAuthClient returns a gRPC client for interacting with the account service.
func NewAuthClient(addr string) (pb.AuthServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return pb.NewAuthServiceClient(conn), conn.Close, nil
}

// NewClientClient returns a gRPC client for interacting with the account service.
func NewClientClient(addr string) (pb.ClientServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return pb.NewClientServiceClient(conn), conn.Close, nil
}
