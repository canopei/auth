package auth

const (
	AuthorizationHeader   = "Authorization"
	AuthorizationMetadata = "authorization"
)

type Token string
