package main

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/canopei/auth/config"
	authPb "bitbucket.org/canopei/auth/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"bitbucket.org/canopei/golibs/slices"
	cStrings "bitbucket.org/canopei/golibs/strings"
	"github.com/golang/protobuf/ptypes/empty"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// MaxClientsFetched is the limit of number of clients that can be fetched
	// at once.
	MaxClientsFetched int32 = 50
)

// CreateClient creates a Client
func (s *Server) CreateClient(ctx context.Context, req *authPb.CreateClientRequest) (*authPb.Client, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClient: %v", req)
	defer timeTrack(logger, time.Now(), "CreateClient")

	// Validations
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	req.RedirectUri = strings.TrimSpace(req.RedirectUri)
	if req.RedirectUri == "" {
		logger.Error("The redirect URI is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The redirect URI is required.")
	}
	req.GrantTypes = strings.TrimSpace(req.GrantTypes)
	if req.GrantTypes == "" {
		logger.Error("The grant types are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The grant types are required.")
	}

	grantTypes, err := parseGrantTypes(req.GrantTypes)
	if err != nil {
		logger.Errorf("%s", err)
		return nil, grpc.Errorf(codes.InvalidArgument, "%s", err)
	}
	if len(grantTypes) == 0 {
		logger.Error("At least one grant type is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one grant type is required.")
	}

	// Create the client
	uuid, err := crypto.NewUUID()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate the UUID.")
	}

	secret := cStrings.Rand(s.Config.SecretLength)
	client := &authPb.Client{
		Uuid:        uuid.String(),
		Secret:      secret,
		SiteId:      req.SiteId,
		GrantTypes:  strings.Join(grantTypes, ","),
		RedirectUri: req.RedirectUri,
	}

	_, err = s.DB.NamedExec(`INSERT INTO oauth_client (uuid, site_id, secret, grant_types, redirect_uri)
		VALUES (unhex(replace(:uuid,'-','')), :site_id, :secret, :grant_types, :redirect_uri)`, client)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry for client.")
	}
	logger.Infof("Created client %s.", client.GetUuid())

	err = s.DB.Get(client, "SELECT * FROM oauth_client WHERE uuid = unhex(replace(?,'-',''))", client.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Client not found after creation: '%s'", client.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the new client.")
	}
	client.Uuid = uuid.String()

	return client, nil
}

// UpdateClient updates a Client
func (s *Server) UpdateClient(ctx context.Context, newClient *authPb.Client) (*authPb.Client, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClient: %v", newClient)
	defer timeTrack(logger, time.Now(), "UpdateClient")

	// Validations
	newClient.Uuid = strings.TrimSpace(newClient.Uuid)
	if newClient.Uuid == "" {
		logger.Error("The redirect URI is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The redirect URI is required.")
	}
	newClient.RedirectUri = strings.TrimSpace(newClient.RedirectUri)
	if newClient.RedirectUri == "" {
		logger.Error("The redirect URI is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The redirect URI is required.")
	}
	newClient.GrantTypes = strings.TrimSpace(newClient.GrantTypes)
	if newClient.GrantTypes == "" {
		logger.Error("The grant types are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The grant types are required.")
	}

	grantTypes, err := parseGrantTypes(newClient.GrantTypes)
	if err != nil {
		logger.Errorf("%s", err)
		return nil, grpc.Errorf(codes.InvalidArgument, "%s", err)
	}
	if len(grantTypes) == 0 {
		logger.Error("At least one grant type is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one grant type is required.")
	}

	_, err = s.DB.NamedExec("UPDATE oauth_client SET redirect_uri = :redirect_uri, grant_types = :grant_types WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))", newClient)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}

	var client authPb.Client
	err = s.DB.Get(&client, "SELECT * FROM oauth_client WHERE uuid = unhex(replace(?,'-',''))", newClient.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClient - client not found: %s", newClient.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	client.Uuid = newClient.Uuid

	return &client, nil
}

// DeleteClient removes a Client
func (s *Server) DeleteClient(ctx context.Context, client *authPb.Client) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClient: %v", client)
	defer timeTrack(logger, time.Now(), "DeleteClient")

	client.Uuid = strings.TrimSpace(client.Uuid)
	if client.Uuid == "" {
		logger.Error("The client UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client UUID is required.")
	}

	logger.Infof("Removing client '%s'....", client.Uuid)
	result, err := s.DB.Exec("UPDATE oauth_client SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))", client.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClient - client not found: '%s'", client.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetClient retrieves a Client by UUID
func (s *Server) GetClient(ctx context.Context, req *authPb.GetClientRequest) (*authPb.Client, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClient: %v", req)
	defer timeTrack(logger, time.Now(), "GetClient")

	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Error("The client UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client UUID is required.")
	}

	var client authPb.Client
	err := s.DB.Get(&client, `SELECT *
		FROM oauth_client
		WHERE uuid = UNHEX(REPLACE(?,'-','')) AND deleted_at = '0'`, req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("GetClient - client not found: '%s'", client.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the client")
	}

	client.Uuid = req.Uuid

	return &client, nil
}

// GetClientByID retrieves a Client by ID
func (s *Server) GetClientByID(ctx context.Context, req *authPb.GetClientByIDRequest) (*authPb.Client, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClientByID: %v", req)
	defer timeTrack(logger, time.Now(), "GetClientByID")

	// Validations
	if req.Id < 1 {
		logger.Error("The client ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client ID is required.")
	}

	var client authPb.Client
	err := s.DB.Get(&client, `SELECT *
		FROM oauth_client
		WHERE client_id = ? AND deleted_at = '0'`, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("GetClientByID - client not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the client")
	}

	unpackedUUID, err := crypto.Parse([]byte(client.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while unpacking the UUID: '%s'", client.Uuid)
	}
	client.Uuid = unpackedUUID.String()

	return &client, nil
}

// GetClients fetches a list of clients
func (s *Server) GetClients(ctx context.Context, req *authPb.GetClientsRequest) (*authPb.ClientsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClients: %v", req)
	defer timeTrack(logger, time.Now(), "GetClients")

	if req.Limit == 0 || req.Limit > MaxClientsFetched {
		req.Limit = 20
	}

	logger.Debugf("Fetching %d clients, offset %d...", req.Limit, req.Offset)

	// fetch the clients
	rows, err := s.DB.Queryx(`SELECT * FROM oauth_client WHERE deleted_at = '0' ORDER BY client_id DESC LIMIT ? OFFSET ?`, req.Limit, req.Offset)
	if err != nil {
		logger.Errorf("Error while fetching the clients: %v", err)
		return nil, err
	}

	clientsList := &authPb.ClientsList{}
	for rows.Next() {
		client := authPb.Client{}
		err := rows.StructScan(&client)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the clients")
		}
		clientsList.Clients = append(clientsList.Clients, &client)
	}

	return clientsList, nil
}

func parseGrantTypes(grantTypes string) ([]string, error) {
	result := []string{}
	reqGrantTypes := strings.Split(grantTypes, ",")
	for _, grant := range reqGrantTypes {
		grant = strings.TrimSpace(grant)
		if grant == "" {
			continue
		}
		if !config.IsGrantTypeValid(grant) {
			return nil, fmt.Errorf("Invalid grant type provided: %s", grant)
		}
		if !slices.Scontains(result, grant) {
			result = append(result, grant)
		}
	}

	return result, nil
}
