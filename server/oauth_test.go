package main

import (
	"context"
	"testing"

	authPb "bitbucket.org/canopei/auth/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	authorizationCodeColumns = []string{"authorization_code_id", "code", "client_id", "account_id", "expires_at", "redirect_uri", "created_at"}
	refreshTokenColumns      = []string{"refresh_token_id", "account_id", "client_id", "expires_at", "token", "created_at"}
)

func TestCreateAuthorizationCodeValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.CreateAuthorizationCodeRequest
		ExpectedCode codes.Code
	}{
		{&authPb.CreateAuthorizationCodeRequest{}, codes.InvalidArgument},
		{&authPb.CreateAuthorizationCodeRequest{RedirectUri: "http://foo.bar", AccountId: 1}, codes.InvalidArgument},
		{&authPb.CreateAuthorizationCodeRequest{ClientId: 1, AccountId: 1}, codes.InvalidArgument},
		{&authPb.CreateAuthorizationCodeRequest{ClientId: 1, RedirectUri: "http://foo.bar"}, codes.InvalidArgument},
		{&authPb.CreateAuthorizationCodeRequest{ClientId: 1, RedirectUri: " ", AccountId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.CreateAuthorizationCodeRequest{ClientId: 1, RedirectUri: "http://foo.bar", AccountId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		authCode, err := server.CreateAuthorizationCode(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(authCode, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateAuthorizationCodeSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT INTO oauth_authorization_code").WillReturnResult(sqlmock.NewResult(1, 1))

	authCode, err := server.CreateAuthorizationCode(context.Background(), &authPb.CreateAuthorizationCodeRequest{
		ClientId:    1,
		RedirectUri: "http://foo.com",
		AccountId:   1,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(authCode)
	if authCode != nil {
		assert.NotEmpty(authCode.Code)
		assert.NotEmpty(authCode.ExpiresAt)
		assert.Equal("http://foo.com", authCode.RedirectUri)
	}
}

func TestGetAuthorizationCodeValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.GetAuthorizationCodeRequest
		ExpectedCode codes.Code
	}{
		{&authPb.GetAuthorizationCodeRequest{}, codes.InvalidArgument},
		{&authPb.GetAuthorizationCodeRequest{Code: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.GetAuthorizationCodeRequest{Code: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		authCode, err := server.GetAuthorizationCode(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(authCode, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetAuthorizationCodeSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(authorizationCodeColumns).AddRow(1, "foo", 2, 3, "2017-01-01", "http://foo.com", "2017-01-01")
	mock.ExpectQuery("^SELECT (.+) FROM oauth_authorization_code").WithArgs("foo").WillReturnRows(rows)

	authCode, err := server.GetAuthorizationCode(context.Background(), &authPb.GetAuthorizationCodeRequest{Code: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(authCode)
	if authCode != nil {
		assert.Equal(int32(3), authCode.AccountId)
	}
}

func TestGetAuthorizationCodeMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM oauth_authorization_code").WithArgs("foo").WillReturnRows(sqlmock.NewRows(authorizationCodeColumns))

	authCode, err := server.GetAuthorizationCode(context.Background(), &authPb.GetAuthorizationCodeRequest{Code: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(authCode)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestRevokeAuthorizationCodeValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.AuthorizationCode
		ExpectedCode codes.Code
	}{
		{&authPb.AuthorizationCode{}, codes.InvalidArgument},
		{&authPb.AuthorizationCode{Code: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.AuthorizationCode{Code: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.RevokeAuthorizationCode(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestRevokeAuthorizationCode(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE oauth_authorization_code SET(.+)expires_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.RevokeAuthorizationCode(context.Background(), &authPb.AuthorizationCode{Code: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE oauth_authorization_code SET(.+)expires_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.RevokeAuthorizationCode(context.Background(), &authPb.AuthorizationCode{Code: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestCreateRefreshTokenValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.CreateRefreshTokenRequest
		ExpectedCode codes.Code
	}{
		{&authPb.CreateRefreshTokenRequest{}, codes.InvalidArgument},
		{&authPb.CreateRefreshTokenRequest{AccountId: 1, ExpiresIn: 3600}, codes.InvalidArgument},
		{&authPb.CreateRefreshTokenRequest{ClientId: 1, ExpiresIn: 3600}, codes.InvalidArgument},
		{&authPb.CreateRefreshTokenRequest{ClientId: 1, AccountId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.CreateRefreshTokenRequest{ClientId: 1, AccountId: 1, ExpiresIn: 3600}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		token, err := server.CreateRefreshToken(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(token, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateRefreshTokenSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT INTO oauth_refresh_token").WillReturnResult(sqlmock.NewResult(1, 1))

	token, err := server.CreateRefreshToken(context.Background(), &authPb.CreateRefreshTokenRequest{
		ClientId:  1,
		AccountId: 2,
		ExpiresIn: 3600,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(token)
	if token != nil {
		assert.NotEmpty(token.Token)
		assert.NotEmpty(token.ExpiresAt)
		assert.Equal(int32(2), token.AccountId)
	}
}

func TestGetRefreshTokenValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.GetRefreshTokenRequest
		ExpectedCode codes.Code
	}{
		{&authPb.GetRefreshTokenRequest{}, codes.InvalidArgument},
		{&authPb.GetRefreshTokenRequest{Token: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.GetRefreshTokenRequest{Token: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		token, err := server.GetRefreshToken(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(token, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetRefreshTokenSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(refreshTokenColumns).AddRow(1, 2, 3, "2017-01-01", "foo", "2017-01-01")
	mock.ExpectQuery("^SELECT (.+) FROM oauth_refresh_token").WithArgs("foo").WillReturnRows(rows)

	token, err := server.GetRefreshToken(context.Background(), &authPb.GetRefreshTokenRequest{Token: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(token)
	if token != nil {
		assert.Equal(int32(2), token.AccountId)
	}
}

func TestGetRefreshTokenMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM oauth_refresh_token").WithArgs("foo").WillReturnRows(sqlmock.NewRows(refreshTokenColumns))

	token, err := server.GetRefreshToken(context.Background(), &authPb.GetRefreshTokenRequest{Token: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(token)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestRevokeRefreshTokenValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.RefreshToken
		ExpectedCode codes.Code
	}{
		{&authPb.RefreshToken{}, codes.InvalidArgument},
		{&authPb.RefreshToken{Token: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.RefreshToken{Token: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.RevokeRefreshToken(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestRevokeRefreshToken(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE oauth_refresh_token SET(.+)expires_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.RevokeRefreshToken(context.Background(), &authPb.RefreshToken{Token: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE oauth_refresh_token SET(.+)expires_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.RevokeRefreshToken(context.Background(), &authPb.RefreshToken{Token: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestCreateAccessTokenValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request        *authPb.CreateAccessTokenRequest
		ExpectedCode   codes.Code
		ExpectNilToken bool
	}{
		{&authPb.CreateAccessTokenRequest{}, codes.InvalidArgument, true},
		{&authPb.CreateAccessTokenRequest{AccountUuid: "uuid", ExpiresIn: 3600}, codes.InvalidArgument, true},
		{&authPb.CreateAccessTokenRequest{ClientId: 1, ExpiresIn: 3600}, codes.InvalidArgument, true},
		{&authPb.CreateAccessTokenRequest{ClientId: 1, AccountUuid: "uuid"}, codes.InvalidArgument, true},
		{&authPb.CreateAccessTokenRequest{ClientId: 1, AccountUuid: " ", ExpiresIn: 3600}, codes.InvalidArgument, true},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.CreateAccessTokenRequest{ClientId: 1, AccountUuid: "uuid", ExpiresIn: 3600}, codes.OK, false},
	}
	for i, validationTest := range validationTests {
		token, err := server.CreateAccessToken(context.Background(), validationTest.Request)
		// we will always get nil
		if validationTest.ExpectNilToken {
			assert.Nil(token, "Failed for case %d.", i)
		} else {
			assert.NotNil(token, "Failed for case %d.", i)
		}
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateAccessToken(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)
	token, err := server.CreateAccessToken(context.Background(), &authPb.CreateAccessTokenRequest{
		ClientId:    1,
		AccountUuid: "uuid",
		ExpiresIn:   3600,
	})

	assert.Nil(err)
	assert.NotNil(token)
	if token != nil {
		assert.NotEmpty(token.Token)
		assert.Equal(int32(3600), token.ExpiresIn)
	}
}
