package main

import (
	"golang.org/x/net/context"

	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
)

// ServerConfig holds the configuration for the server
type ServerConfig struct {
	JWTSigningKey string
	SecretLength  int
}

// Server is the auth service implementation
type Server struct {
	Logger *logrus.Entry
	DB     *sqlx.DB
	Config *ServerConfig
}

// NewServer creates a new Server instance
func NewServer(logger *logrus.Entry, db *sqlx.DB, config *ServerConfig) *Server {
	return &Server{
		Logger: logger,
		DB:     db,
		Config: config,
	}
}

// Ping is used for healthchecks
func (s *Server) Ping(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	return &empty.Empty{}, nil
}
