package main

import (
	"database/sql"
	"strings"
	"time"

	"golang.org/x/net/context"

	authPb "bitbucket.org/canopei/auth/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	cStrings "bitbucket.org/canopei/golibs/strings"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// AuthorizationCodeLength is the length of the authorization code
	AuthorizationCodeLength = 40

	// AuthorizationCodeTTL is the TTL of the authorization code
	AuthorizationCodeTTL = 60 * time.Second

	// RefreshTokenLength is the length of the refresh token
	RefreshTokenLength = 40
)

// CreateAuthorizationCode creates an AuthorizationCode
func (s *Server) CreateAuthorizationCode(ctx context.Context, req *authPb.CreateAuthorizationCodeRequest) (*authPb.AuthorizationCode, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateAuthorizationCode: %v", req)
	defer timeTrack(logger, time.Now(), "CreateAuthorizationCode")

	// Validations
	if req.ClientId < 1 {
		logger.Error("The client ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client ID is required.")
	}
	req.RedirectUri = strings.TrimSpace(req.RedirectUri)
	if req.RedirectUri == "" {
		logger.Error("The redirect URI is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The redirect URI is required.")
	}
	if req.AccountId < 1 {
		logger.Error("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}

	// Create the code
	authCode := &authPb.AuthorizationCode{
		ClientId:    req.ClientId,
		AccountId:   req.AccountId,
		RedirectUri: req.RedirectUri,
		Code:        cStrings.Rand(AuthorizationCodeLength),
		ExpiresAt:   time.Now().Add(AuthorizationCodeTTL).Format(time.RFC3339),
	}

	_, err := s.DB.NamedExec(`INSERT INTO oauth_authorization_code (code, client_id, account_id, redirect_uri, expires_at)
		VALUES (:code, :client_id, :account_id, :redirect_uri, :expires_at)`, authCode)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry for authorization code.")
	}
	logger.Debugf("Created authorization code '%s'", authCode.Code)

	return authCode, nil
}

// GetAuthorizationCode retrieves an AuthorizationCode
func (s *Server) GetAuthorizationCode(ctx context.Context, req *authPb.GetAuthorizationCodeRequest) (*authPb.AuthorizationCode, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetAuthorizationCode: %v", req)
	defer timeTrack(logger, time.Now(), "GetAuthorizationCode")

	// Validations
	req.Code = strings.TrimSpace(req.Code)
	if req.Code == "" {
		logger.Error("The code is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The code is required.")
	}

	var authCode authPb.AuthorizationCode
	err := s.DB.Get(&authCode, "SELECT * FROM oauth_authorization_code WHERE code = ?", req.Code)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("GetAuthorizationCode - code not found '%s'", req.Code)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the authorization code")
	}

	return &authCode, nil
}

// RevokeAuthorizationCode sets expires_at = NOW() for an AuthorizationCode, invalidating it
func (s *Server) RevokeAuthorizationCode(ctx context.Context, code *authPb.AuthorizationCode) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("RevokeAuthorizationCode: %v", code)
	defer timeTrack(logger, time.Now(), "RevokeAuthorizationCode")

	// Validations
	code.Code = strings.TrimSpace(code.Code)
	if code.Code == "" {
		logger.Error("The code is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The code is required.")
	}

	logger.Debugf("Revoking code '%s'....", code.Code)
	result, err := s.DB.Exec("UPDATE oauth_authorization_code SET expires_at = CURRENT_TIMESTAMP WHERE code = ?", code.Code)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("RevokeAuthorizationCode - code not found: '%s'", code.Code)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// CreateRefreshToken creates a RefreshToken
func (s *Server) CreateRefreshToken(ctx context.Context, req *authPb.CreateRefreshTokenRequest) (*authPb.RefreshToken, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateRefreshToken: %v", req)
	defer timeTrack(logger, time.Now(), "CreateRefreshToken")

	// Validations
	if req.ClientId < 1 {
		logger.Error("The client ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client ID is required.")
	}
	if req.AccountId < 1 {
		logger.Error("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	if req.ExpiresIn < 1 {
		logger.Error("The expiring interval is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The expiring interval is required.")
	}

	// Create the code
	refreshToken := &authPb.RefreshToken{
		ClientId:  req.ClientId,
		AccountId: req.AccountId,
		Token:     cStrings.Rand(RefreshTokenLength),
		ExpiresAt: time.Now().Add(time.Duration(req.ExpiresIn) * time.Second).Format(time.RFC3339),
	}

	_, err := s.DB.NamedExec(`INSERT INTO oauth_refresh_token (token, client_id, account_id, expires_at)
		VALUES (:token, :client_id, :account_id, :expires_at)`, refreshToken)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry for refresh token.")
	}
	logger.Debugf("Created refresh token '%s'", refreshToken.Token)

	return refreshToken, nil
}

// RevokeRefreshToken sets expires_at = NOW() for a RefreshToken, invalidating it
func (s *Server) RevokeRefreshToken(ctx context.Context, refreshToken *authPb.RefreshToken) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("RevokeRefreshToken: %v", refreshToken)
	defer timeTrack(logger, time.Now(), "RevokeRefreshToken")

	// Validations
	refreshToken.Token = strings.TrimSpace(refreshToken.Token)
	if refreshToken.Token == "" {
		logger.Error("The token is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The token is required.")
	}

	logger.Infof("Revoking token '%s'....", refreshToken.Token)
	result, err := s.DB.Exec("UPDATE oauth_refresh_token SET expires_at = CURRENT_TIMESTAMP WHERE token = ?", refreshToken.Token)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("RevokeRefreshToken - token not found: '%s'", refreshToken.Token)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetRefreshToken retrives a refresh token
func (s *Server) GetRefreshToken(ctx context.Context, req *authPb.GetRefreshTokenRequest) (*authPb.RefreshToken, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetRefreshToken: %v", req)
	defer timeTrack(logger, time.Now(), "GetRefreshToken")

	// Validations
	req.Token = strings.TrimSpace(req.Token)
	if req.Token == "" {
		logger.Error("The token is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The token is required.")
	}

	var refreshToken authPb.RefreshToken
	err := s.DB.Get(&refreshToken, "SELECT * FROM oauth_refresh_token WHERE token = ?", req.Token)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("GetRefreshToken - token not found '%s'", req.Token)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the refresh token")
	}

	return &refreshToken, nil
}

// CreateAccessToken creates an AccessToken
func (s *Server) CreateAccessToken(ctx context.Context, req *authPb.CreateAccessTokenRequest) (*authPb.AccessToken, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateAccessToken: %v", req)
	defer timeTrack(logger, time.Now(), "CreateAccessToken")

	// Validations
	if req.ClientId < 1 {
		logger.Error("The client ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The client ID is required.")
	}
	req.AccountUuid = strings.TrimSpace(req.AccountUuid)
	if len(req.AccountUuid) == 0 {
		logger.Error("The account UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account UUID is required.")
	}
	if req.ExpiresIn < 1 {
		logger.Error("The expiring interval is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The expiring interval is required.")
	}

	// build the JWT token
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(time.Duration(req.ExpiresIn) * time.Second).Unix()
	claims["client_id"] = req.ClientId
	claims["account_uuid"] = req.AccountUuid
	claims["account_permissions"] = req.AccountPermissions

	ss, err := token.SignedString([]byte(s.Config.JWTSigningKey))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot sign the access token")
	}

	return &authPb.AccessToken{Token: ss, ExpiresIn: req.ExpiresIn}, nil
}
