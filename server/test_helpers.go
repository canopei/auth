package main

import (
	"io/ioutil"

	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
)

// NewServerMock creates a mock of Server
func NewServerMock(db *sqlx.DB) *Server {
	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	config := &ServerConfig{
		JWTSigningKey: "randomkey",
		SecretLength:  16,
	}

	return NewServer(logger, db, config)
}
