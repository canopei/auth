package main

import (
	"time"

	"github.com/Sirupsen/logrus"
)

func timeTrack(logger *logrus.Entry, start time.Time, name string) {
	elapsed := time.Since(start)
	logger.Debugf("%s took %s", name, elapsed)
}
