package main

import (
	"context"
	"strings"
	"testing"

	authPb "bitbucket.org/canopei/auth/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	clientColumns = []string{"client_id", "uuid", "site_id", "secret", "grant_types", "redirect_uri", "created_at", "modified_at", "deleted_at"}
)

func TestCreateClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.CreateClientRequest
		ExpectedCode codes.Code
	}{
		{&authPb.CreateClientRequest{}, codes.InvalidArgument},
		{&authPb.CreateClientRequest{SiteId: 1, GrantTypes: "implicit, password"}, codes.InvalidArgument},
		{&authPb.CreateClientRequest{SiteId: 1, RedirectUri: "http://foo.com"}, codes.InvalidArgument},
		{&authPb.CreateClientRequest{RedirectUri: "http://foo.com", GrantTypes: "implicit, password"}, codes.InvalidArgument},
		{&authPb.CreateClientRequest{SiteId: 1, RedirectUri: "http://foo.com", GrantTypes: ", "}, codes.InvalidArgument},
		{&authPb.CreateClientRequest{SiteId: 1, RedirectUri: "http://foo.com", GrantTypes: ", "}, codes.InvalidArgument},
		{&authPb.CreateClientRequest{SiteId: 1, RedirectUri: "http://foo.com", GrantTypes: "implicit,something"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.CreateClientRequest{SiteId: 1, RedirectUri: "http://foo.com", GrantTypes: "implicit, password"}, codes.Unknown},
		{&authPb.CreateClientRequest{SiteId: 1, RedirectUri: "http://foo.com", GrantTypes: "implicit, "}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		client, err := server.CreateClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(client, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClientSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT INTO oauth_client").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(clientColumns).AddRow(
		1, "1234567890123456", 1, "secret", "implicit,password", "http://foo.com", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WillReturnRows(rows)

	client, err := server.CreateClient(context.Background(), &authPb.CreateClientRequest{
		SiteId:      1,
		RedirectUri: "http://foo.com",
		GrantTypes:  "implicit,password",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(client)
	if client != nil {
		assert.NotEmpty(client.Secret)
		assert.Equal("http://foo.com", client.RedirectUri)
		assert.Len(strings.Split(client.GrantTypes, ","), 2)
	}
}

func TestGetClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.GetClientRequest
		ExpectedCode codes.Code
	}{
		{&authPb.GetClientRequest{}, codes.InvalidArgument},
		{&authPb.GetClientRequest{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.GetClientRequest{Uuid: "uuid"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		client, err := server.GetClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(client, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(clientColumns).AddRow(
		1, "1234567890123456", 1, "secret", "implicit,password", "http://foo.com", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WithArgs("uuid").WillReturnRows(rows)

	client, err := server.GetClient(context.Background(), &authPb.GetClientRequest{Uuid: "uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(client)
	if client != nil {
		assert.Equal("implicit,password", client.GrantTypes)
	}
}

func TestGetClientMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WithArgs("uuid").WillReturnRows(sqlmock.NewRows(clientColumns))

	client, err := server.GetClient(context.Background(), &authPb.GetClientRequest{Uuid: "uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(client)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClientByIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.GetClientByIDRequest
		ExpectedCode codes.Code
	}{
		{&authPb.GetClientByIDRequest{}, codes.InvalidArgument},
		{&authPb.GetClientByIDRequest{Id: -2}, codes.InvalidArgument},
		{&authPb.GetClientByIDRequest{Id: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.GetClientByIDRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		client, err := server.GetClientByID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(client, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClientByIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(clientColumns).AddRow(
		1, "1234567890123456", 1, "secret", "implicit,password", "http://foo.com", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WithArgs(1).WillReturnRows(rows)

	client, err := server.GetClientByID(context.Background(), &authPb.GetClientByIDRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(client)
	if client != nil {
		assert.Equal("implicit,password", client.GrantTypes)
	}
}

func TestGetClientByIDMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WithArgs(1).WillReturnRows(sqlmock.NewRows(clientColumns))

	client, err := server.GetClientByID(context.Background(), &authPb.GetClientByIDRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(client)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClientsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	clients := []*authPb.Client{
		&authPb.Client{Id: 1, Uuid: "1234567890123456", SiteId: 1, Secret: "secret1", GrantTypes: "password", RedirectUri: "http://foo.com"},
		&authPb.Client{Id: 2, Uuid: "1234567890123457", SiteId: 2, Secret: "secret2", GrantTypes: "password", RedirectUri: "http://foo.com"},
		&authPb.Client{Id: 3, Uuid: "1234567890123458", SiteId: 3, Secret: "secret3", GrantTypes: "password", RedirectUri: "http://bar.com"},
	}
	clientsRows := sqlmock.NewRows(clientColumns)
	for _, pr := range clients {
		clientsRows.AddRow(pr.Id, pr.Uuid, pr.SiteId, pr.Secret, pr.GrantTypes, pr.RedirectUri, "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WillReturnRows(clientsRows)

	response, err := server.GetClients(context.Background(), &authPb.GetClientsRequest{Limit: 20, Offset: 10})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Clients, len(clients))
		// Sanity check
		assert.Equal("secret2", response.Clients[1].Secret)
	}
}

func TestGetClientsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WillReturnRows(sqlmock.NewRows(clientColumns))

	response, err := server.GetClients(context.Background(), &authPb.GetClientsRequest{Limit: 20, Offset: 10})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Clients, 0)
	}
}

func TestUpdateClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.Client
		ExpectedCode codes.Code
	}{
		{&authPb.Client{}, codes.InvalidArgument},
		{&authPb.Client{Uuid: "uuid", GrantTypes: "implicit, password"}, codes.InvalidArgument},
		{&authPb.Client{Uuid: "uuid", RedirectUri: "http://foo.com"}, codes.InvalidArgument},
		{&authPb.Client{GrantTypes: "implicit, password", RedirectUri: "http://foo.com"}, codes.InvalidArgument},
		{&authPb.Client{Uuid: " ", GrantTypes: "implicit, password", RedirectUri: "http://foo.com"}, codes.InvalidArgument},
		{&authPb.Client{Uuid: "uuid", RedirectUri: "http://foo.com", GrantTypes: ", "}, codes.InvalidArgument},
		{&authPb.Client{Uuid: "uuid", RedirectUri: "http://foo.com", GrantTypes: ", "}, codes.InvalidArgument},
		{&authPb.Client{Uuid: "uuid", RedirectUri: "http://foo.com", GrantTypes: "implicit,something"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.Client{Uuid: "uuid", RedirectUri: "http://foo.com", GrantTypes: "implicit, password"}, codes.Unknown},
		{&authPb.Client{Uuid: "uuid", RedirectUri: "http://foo.com", GrantTypes: "implicit, "}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		client, err := server.UpdateClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(client, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClientMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClient := &authPb.Client{
		Uuid:        "uuid",
		RedirectUri: "http://foo.com",
		GrantTypes:  "implicit, password",
	}

	mock.ExpectExec("^UPDATE oauth_client SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM oauth_client").WithArgs("uuid").WillReturnRows(sqlmock.NewRows(clientColumns))

	client, err := server.UpdateClient(context.Background(), testClient)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(client)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClientSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClient := &authPb.Client{
		Uuid:        "uuid",
		RedirectUri: "http://foo.com",
		GrantTypes:  "implicit, password",
	}

	mock.ExpectExec("^UPDATE oauth_client SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(clientColumns).AddRow(
		1, "1234567890123456", 1, "secret", "implicit,password", "http://foo.com", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM oauth_client WHERE").WithArgs(testClient.Uuid).WillReturnRows(rows)

	client, err := server.UpdateClient(context.Background(), testClient)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(client)
	// sanity check
	if client != nil {
		assert.Equal("implicit,password", client.GrantTypes)
	}
}

func TestDeleteClientValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *authPb.Client
		ExpectedCode codes.Code
	}{
		{&authPb.Client{}, codes.InvalidArgument},
		{&authPb.Client{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&authPb.Client{Uuid: "uuid"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteClient(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClient(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE oauth_client SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClient(context.Background(), &authPb.Client{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE oauth_client SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteClient(context.Background(), &authPb.Client{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
